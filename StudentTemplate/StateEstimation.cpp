//-----------------------------------------------------------------------------
//                           State Estimation
//-----------------------------------------------------------------------------
//
//  File:     StateEstimation.cpp
//  Project:  UCF Rocket Powered Glider
//  Author:   Jake Covington & Jordan Street
//
//-----------------------------------------------------------------------------
//  The MIT License (MIT)
//  
//  Copyright (c) 2019 Jacob H. Covington & Jordan T. Street
// 
//  Permission is hereby granted, free of charge, to any person obtaining a 
//  copy of this software and associated documentation files (the "Software"), 
//  to deal in the Software without restriction, including without limitation 
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//  and/or sell copies of the Software, and to permit persons to whom the 
//  Software is furnished to do so, subject to the following conditions:
// 
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
// 
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
//  DEALINGS IN THE SOFTWARE.
//-----------------------------------------------------------------------------

// Includes
#include "StateEstimation.h"
#include "MKRIMU.h"

//-----------------------------------------------------------------------------
// Function: Constructor
//-----------------------------------------------------------------------------
StateEstimation::StateEstimation(void)
{
  // Do nothing
}

//-----------------------------------------------------------------------------
// Function: Destructor
//-----------------------------------------------------------------------------
StateEstimation::~StateEstimation(void)
{
  // Do nothing
}

//-----------------------------------------------------------------------------
// Function: Init
//-----------------------------------------------------------------------------
void StateEstimation::Init(int filterRate, bool useMag_)
{
  // IMU error check
  if (!IMU.begin()){
    Serial.println("Failed to initialize IMU!");
    while (1); // Stay here forever - requires board reset to start over
  }

  // Init MW filter with the user specified update rate
  mwFilter.begin(filterRate);

  // Set flag to use Magnetometer
  useMag = useMag_;

  // Init values
  gx = 0;
  gy = 0;
  gz = 0;
  ax = 0;
  ay = 0;
  az = 0;
  mx = 0;
  my = 0;
  mz = 0;
}

//-----------------------------------------------------------------------------
// Function: Update
//-----------------------------------------------------------------------------
void StateEstimation::Update(void)
{
  // Read in data from IMU
  IMU.readGyroscope(gx,gy,gz);
  IMU.readAcceleration(ax,ay,az);
  if (useMag){
    IMU.readMagneticField(mx,my,mz);
  }

  // Remap the inputs to account for IMU orientation on the board
  Gx =  gy; // p (roll rate)  [deg/s]
  Gy =  gx; // q (pitch rate) [deg/s]
  Gz = -gz; // r (yaw rate)   [deg/s]
  Ax = -ay; // ax (body +x)   [G's]
  Ay = -ax; // ay (body +y)   [G's]
  Az =  az; // az (body +z)   [G's]
  Mx = -my; // mx (mag +x)    [uT]
  My = -mx; // my (mag +y)    [uT]
  Mz =  mz; // mz (mag +z)    [uT]
  
  // Update the MW filter
  if (useMag) {
    mwFilter.update(Gx,Gy,Gz,Ax,Ay,Az,Mx,My,Mz);
  }
  else {
    mwFilter.updateIMU(Gx,Gy,Gz,Ax,Ay,Az);
  }

  // Grab Euler angles
  yaw   = mwFilter.getYaw();
  pitch = mwFilter.getPitch();
  roll  = mwFilter.getRoll();
}

//-----------------------------------------------------------------------------
// Function: Debug
//-----------------------------------------------------------------------------
void StateEstimation::Debug(bool debug2SM, bool debug2SD, File dataLog)
{ 
  // Construct debug string
  String debugString = "";
  debugString += String(yaw);
  debugString += ",";
  debugString += String(pitch);
  debugString += ",";
  debugString += String(roll);
  debugString += ",";

  // Print to serial monitor (if specified)
  if (Serial && debug2SM){
    Serial.println(debugString);
  }
  
  // Print to SD card buffer (if specified)
  // NOTE: You may want to reduce writing to the SD card every timestep as this
  // can severly slow down the Arduino which can screw up your control system
  if (dataLog && debug2SD){
    dataLog.println(debugString);
  }
}

//-----------------------------------------------------------------------------
// EOF
