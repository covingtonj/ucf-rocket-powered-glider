//-----------------------------------------------------------------------------
//                           State Estimation
//-----------------------------------------------------------------------------
//
//  File:     StateEstimation.h
//  Project:  UCF Rocket Powered Glider
//  Author:   Jake Covington & Jordan Street
//
//-----------------------------------------------------------------------------
//  The MIT License (MIT)
//  
//  Copyright (c) 2019 Jacob H. Covington & Jordan T. Street
// 
//  Permission is hereby granted, free of charge, to any person obtaining a 
//  copy of this software and associated documentation files (the "Software"), 
//  to deal in the Software without restriction, including without limitation 
//  the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//  and/or sell copies of the Software, and to permit persons to whom the 
//  Software is furnished to do so, subject to the following conditions:
// 
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
// 
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
//  DEALINGS IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#ifndef StateEstimation_H
#define StateEstimation_H

#include "MadgwickAHRS.h"
#include <SD.h>

class StateEstimation {

private:
  bool useMag;
  float gx,gy,gz,ax,ay,az,mx,my,mz;
  
  Madgwick mwFilter;

public:
  StateEstimation(void);
  ~StateEstimation(void);
  void Init(int filterRate, bool useMag_);
  void Update(void);
  void Debug(bool debug2SM, bool debug2SD, File dataLog);

  float yaw, pitch, roll;
  float Gx,Gy,Gz,Ax,Ay,Az,Mx,My,Mz;
};

#endif // StateEstimation_H

//-----------------------------------------------------------------------------
// EOF