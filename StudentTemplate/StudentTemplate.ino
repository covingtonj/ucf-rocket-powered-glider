//-----------------------------------------------------------------------------
//                            Student Template
//-----------------------------------------------------------------------------
//
//  File:     StudentTemplate.ino
//  Project:  UCF Rocket Powered Glider
//  Author:   Jake Covington & Jordan Street
//
//-----------------------------------------------------------------------------
// Function:
//   This serves as the main template for the rocket powered glider software.
//   All code is entirely up to you on how you use it so change as necessary.
//   The template is a skeleton made up of comments (denoted by "//") meaning
//   these are for reference and won't get compiled in. COMMENT WELL - this
//   will ultimately benefit you and your team to understand what each section
//   or inidivdual line of code is doing. A word of advice is to write your own
//   functions/libraries as needed (see the Arduino MKR zero examples for help
//   or just Google it) - this will keep your "loop" function SHORT and CLEAN.
//   Lastly, before an actual flight, comment out all of the continuous serial
//   prints as these take up pointless clock cycles that slow down the Arduino.
//-----------------------------------------------------------------------------

// Includes
#include <SD.h>
#include <Servo.h>
#include "StateEstimation.h"

// Variable inits
StateEstimation navFilter; // Create state estimation object
Servo elServo;             // Create servo object for elevator
char logName[50];
int dataLogNum = -1;
File dataLog;
float elCmd = 180;
float tempTime; // Temp variable

// User inits
const int chipSelect = SDCARD_SS_PIN;
bool debug2SM = true;  // Write debug statements to serial monitor
bool debug2SD = true;  // Write debug statements to SD card
bool debugNav = true;  // Write out nav debug statements

//-----------------------------------------------------------------------------
// Function: Setup - this function only runs once at power on / reset
//-----------------------------------------------------------------------------
void setup() {
  // --------------------------------------------------------------------
  // Initialize processor functions as needed - comms. protocols, etc.
  // --------------------------------------------------------------------
  
  // Init serial port for debug outputs to serial monitor (Tools > Serial Monitor)
  if (debug2SM) {
    Serial.begin(9600); // Set serial port to 9600 baud rate
    while (!Serial);    // Wait for serial port to open up
  }

  // --------------------------------------------------------------------
  // Set outputs to default values (servos, LEDs, telemetry, etc.)
  // --------------------------------------------------------------------
  elServo.attach(9);    // Attach the servo object to pin 9 on the MKR board
  elServo.write(elCmd); // Command servo to some default position

  // --------------------------------------------------------------------
  // Initialize state machine, nav (state est.), guidance, and control loops
  // --------------------------------------------------------------------
  navFilter.Init(100,false); // Init nav filter @ 100 Hz & don't use the magnetometer

  // --------------------------------------------------------------------
  // Any other stuff you wanna do before the main loop starts (up to you)
  // --------------------------------------------------------------------
  // Check for errors in SD card init - if not, open a new log
  if (debug2SD)
  {
    Serial.print("Initializing SD card...");
    
    if (!SD.begin(chipSelect)){
      Serial.println("card failed or not present!");
      debug2SD = false; // Set to false so nothing attempts to write to the card
    }
    Serial.println("card initialized.");

    // Check if any data logs exist and increment counter for new one
    do {
      sprintf(logName, "datalog%d.txt",++dataLogNum);
    } while (SD.exists(logName));

    // Open the log for writing
    sprintf(logName, "datalog1.txt");
    dataLog = SD.open(logName, FILE_WRITE);
  }

  // This is a temporary thing used for the servo demo below (not needed for your code)
  tempTime = millis();

  Serial.println("Done with setup!");
}

//-----------------------------------------------------------------------------
// Function: Loop - the "main" loop that continuously runs until power off
//-----------------------------------------------------------------------------
void loop() {

  // Navigation (state estimation) Whatever way your are processing your sensor
  // data should be dealt with here and the output should be readily available
  // for use in the state machine, guidance, and control sections
  navFilter.Update();
  
  // State Machine (run your state machine) - This should keep track of what
  // phase of flight your vehicle is in. HINT: Use case statements
  // Example Phases: Sitting on launcher, boost, apogee reached, glide, landed
  
  
  // Guidance (self explanatory) - You know where you are from the nav, you know
  // what you should be doing from what "state" or phase of flight your are in,
  // now determine what the input to the autopilot (control system) should be.
  // This really is an outer control loop around the autopilot
  
  
  // Control (autopilot) - take guidance command and determine control surface
  // commands, i.e. the servo positions. Remember that the servo positions need
  // to be converted to the appropriate PWM signal. NOTE: To access the Euler angles
  // simply call navFilter.<EULER_ANGLE> -> Example: roll = navFilter.roll;

  // Example for how to command the servo to rotate from side to side every 5 seconds
  if ((millis() - tempTime) > 5000)
  {
    if (elCmd > 90){
      elCmd = 0;
    }
    else{
      elCmd = 180;
    }
    tempTime = millis(); // Update timestamp
  }
  elServo.write(elCmd); // Write elevator deflection command out to the servo
  
  // Telemetry (debug) - if needed, write out values here to the serial monitor
  // Only the SD card can be used for real flight unless you have a wireless 
  // downlink on board so be sure to set debug2SM = false when not using a serial
  // monitor as the code is currently set to wait for this to open before proceeding
  
  // Go through debug function for each module
  if (debugNav){
    navFilter.Debug(debug2SM,debug2SD,dataLog);
  }
  
  // Write to SD card (if specified)
  // NOTE: You may want to reduce writing to the SD card every timestep as this
  // can severly slow down the Arduino which can screw up your control system
  if (dataLog && debug2SD){
    dataLog.flush();
  }

}

// ------------------------------------------------------------------------
// Extra Functions Here
// ------------------------------------------------------------------------



//-----------------------------------------------------------------------------
// EOF
